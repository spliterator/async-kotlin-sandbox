package com.butilov.async.benchmark

import com.butilov.async.*
import org.openjdk.jmh.annotations.*
import java.util.concurrent.TimeUnit

@Fork(1)
@Warmup(iterations = 10, time = 2, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Benchmark)
open class ReactorBenchmark {

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    open fun runReactive() = runParallelNonBlocking()

}