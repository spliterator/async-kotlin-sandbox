package com.butilov.async.benchmark

import org.openjdk.jmh.Main

/**
 *  Benchmark                          Mode  Cnt     Score     Error  Units
 *  AkkaBenchmark.runActors            avgt    5  2496.280 ± 255.996  ms/op
 *  CoroutinesBenchmark.runCoroutines  avgt    5  1288.284 ±  13.491  ms/op
 *  ReactorBenchmark.runReactive       avgt    5  1697.784 ± 179.808  ms/op
 */
fun main(args: Array<String>) {
    Main.main(args)
}
