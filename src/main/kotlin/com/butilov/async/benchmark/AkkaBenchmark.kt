package com.butilov.async.benchmark

import akka.actor.Actor.noSender
import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.Props
import akka.routing.FromConfig
import com.butilov.async.DotActor
import com.butilov.async.SupervisorActor
import com.butilov.async.cmd_run
import com.typesafe.config.ConfigFactory
import org.openjdk.jmh.annotations.*
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit

@Fork(1)
@Warmup(iterations = 10, time = 2, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 5, time = 2, timeUnit = TimeUnit.MILLISECONDS)
@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Benchmark)
open class AkkaBenchmark {

    @Setup(Level.Invocation)
    fun setUp() {
        actorSystem = ActorSystem.create("ActorSystem", actorConfig)
        dotActorRef = actorSystem.actorOf(FromConfig.getInstance().props(Props.create(DotActor::class.java)), "dot")
        completableFuture = CompletableFuture<Boolean>()
        supervisorActorRef = actorSystem.actorOf(
            Props.create(SupervisorActor::class.java) { SupervisorActor(dotActorRef, completableFuture) }, "supervisor"
        )
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    open fun runActors() {
        supervisorActorRef.tell(cmd_run, noSender())
        while (!completableFuture.isDone) {
            // wait
        }
    }

    @TearDown(Level.Invocation)
    open fun doTearDown() {
        actorSystem.terminate()
        Await.ready(actorSystem.whenTerminated(), Duration.Inf())
    }

    companion object {
        // @formatter:off
        @JvmStatic private val actorConfig = ConfigFactory.load(ClassLoader.getSystemClassLoader(), "akka.conf")
        @JvmStatic private lateinit var actorSystem: ActorSystem
        @JvmStatic private lateinit var dotActorRef: ActorRef
        @JvmStatic private lateinit var supervisorActorRef: ActorRef
        @JvmStatic private lateinit var completableFuture: CompletableFuture<Boolean>
        // @formatter:on
    }

}