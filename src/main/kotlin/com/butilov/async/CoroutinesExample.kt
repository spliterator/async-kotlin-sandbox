package com.butilov.async

import kotlinx.coroutines.*
import kotlin.concurrent.thread

fun runOldPlainThreads() = runBlocking {
    repeat(100_000) {
        thread {
            sleepBlockingAndPrint()
        }
    }
}

fun launchCoroutines() = runBlocking {
    repeat(100_000) { // launch a lot of coroutines
        launch {
            sleepNonblockingAndPrint()
        }
    }
}

fun launchCoroutinesWithBlocking() = runBlocking {
    repeat(100_000) {
        launch {
            sleepBlockingAndPrint()
        }
    }
}

suspend fun asyncAwaitCoroutinesWithNonBlocking() = coroutineScope {
    (1..100_000)
        .map { async { sleepNonblockingAndPrint() } }
        .awaitAll()
}

private fun sleepBlockingAndPrint() = Thread.sleep(1000).also { print(".") }

private suspend fun sleepNonblockingAndPrint() = delay(1000).also { print(".") }
